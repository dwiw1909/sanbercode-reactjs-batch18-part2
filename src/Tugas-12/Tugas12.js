import React, { Component } from 'react';
import TableItem from './TableItem';
import './Tugas12.css';

class Tugas12 extends Component {
  constructor(){
    super()
    this.state = {
      dataHargaBuah : [
        {nama: "Semangka", harga: 10000, berat: 1000},
        {nama: "Anggur", harga: 40000, berat: 500},
        {nama: "Strawberry", harga: 30000, berat: 400},
        {nama: "Jeruk", harga: 30000, berat: 1000},
        {nama: "Mangga", harga: 30000, berat: 500}
      ],
      form: {
        nama: '',
        harga: '',
        berat: '',
      },
      edit: false,
      editKey: null
    }
  }

  clear = () => {
    this.setState({
      form: {
        nama: '',
        harga: '',
        berat: ''
      },
      edit: false,
      editKey: null
    })
  }

  onChange = (key, val) => {
    this.setState({
      form: {
        ...this.state.form,
        [key]: val
      }
    })
  }

  save = (event) => {
    event.preventDefault()
    if(this.state.form.name !== '' && this.state.form.harga !== '' && this.state.form.berat !== ''){
      if(!this.state.edit){
        let newObj = {nama: this.state.form.nama, harga: this.state.form.harga, berat: this.state.form.berat}
        this.setState({
          dataHargaBuah: [...this.state.dataHargaBuah, newObj]
        })
        this.clear()
      } else {
        console.log('edit nih' + this.state.editKey );
        let datas = [...this.state.dataHargaBuah]
        datas.splice(this.state.editKey, 1, this.state.form)
        this.setState({
          dataHargaBuah: [...datas]
        })
        this.clear()
      }
    } else {
      alert('input form harus di isi semua')
    }
  }

  del = (key) => {
    let datas = this.state.dataHargaBuah
    datas.splice(key, 1)
    this.setState({
      dataHargaBuah: [...datas]
    })
  }

  handleEdit = (key) => {
    this.setState({
      edit: true,
      editKey: key,
      form: this.state.dataHargaBuah[key]
    })
  }

  render() {
    return (
      <div className='container'>
        <h2>Form Input</h2>
        <div className='form'>
          <form onSubmit={this.save}>
            <label>Nama</label>
            <input type='text' value={this.state.form.nama|| ''} onChange={value => this.onChange('nama', value.target.value)}/><br /><br />
            <label>Harga</label>
            <input type='text' value={this.state.form.harga|| ''} onChange={value => this.onChange('harga', value.target.value)} /><br /><br />
            <label>Berat</label>
            <input type='text' value={this.state.form.berat|| ''} onChange={value => this.onChange('berat', value.target.value)} /> gram<br /><br />
            <div style={{display: 'flex'}}>
            {
              this.state.edit
              ?<input className='submitBtn' type='submit' value='Edit' />
              :<input className='submitBtn' type='submit' value='Simpan' />
            }
            </div>
          </form>
        </div>
        <h2>Table Harga Buah</h2>
        <div className='body'>
          <table className='tugas10'>
            <thead>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {this.state.dataHargaBuah.map((data, index) => (
                <TableItem item={data} index={index} key={index} del={() => this.del(index)} edit={() => this.handleEdit(index)} />)
              )}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default Tugas12;