import React from 'react'

const TableItem = ({item, index, del, edit}) => {
  return (
    <tr>
      <td>{item.nama}</td>
      <td>{item.harga}</td>
      <td>{item.berat/1000} kg</td>
      <td style={{width: '100px', alignItems: 'center'}}>
        <button type='button' onClick={() => edit()}>Edit</button>
        <button type='button' onClick={() => del()}>Hapus</button>
      </td>
    </tr>
  )
}

export default TableItem
