import React from 'react'

const TableItem = ({item}) => {
  return (
    <tr>
      <td>{item.nama}</td>
      <td>{item.harga}</td>
      <td>{item.berat/1000} kg</td>
    </tr>
  )
}

export default TableItem
