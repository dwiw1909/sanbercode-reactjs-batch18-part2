import React from 'react'
import './Tugas10.css'
import TableItem from './TableItem'

const Tugas10 = () => {
  let dataHargaBuah = [
    {nama: "Semangka", harga: 10000, berat: 1000},
    {nama: "Anggur", harga: 40000, berat: 500},
    {nama: "Strawberry", harga: 30000, berat: 400},
    {nama: "Jeruk", harga: 30000, berat: 1000},
    {nama: "Mangga", harga: 30000, berat: 500}
  ]
  return (
    <div className='container'>
      <h2>Table Harga Buah</h2>
      <div className='body'>
        <table className='tugas10'>
          <tr>
            <th>Nama</th>
            <th>Harga</th>
            <th>Berat</th>
          </tr>
          {dataHargaBuah.map(data => (<TableItem item={data} />))}
        </table>
      </div>
    </div>
  )
}

export default Tugas10
