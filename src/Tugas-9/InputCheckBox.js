import React from 'react'

const InputCheckBox = ({name, val}) => {
  return (
    <div>
      <input type="checkbox" name={name} value={val} />
      <label for={name}>{val}</label>
    </div>
  )
}

export default InputCheckBox;