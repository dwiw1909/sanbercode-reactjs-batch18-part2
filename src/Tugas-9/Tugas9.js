import React from 'react'
import './Tugas9.css';
import InputCheckBox from './InputCheckBox'

const Tugas9 = () => {
  return (
    <div>
      <div className='container'>
      <h1 style={{textAlign: 'center'}}>Form Pembelian Buah</h1>
      <form>
        <table style={{borderSpacing: '10px'}} className='tugas9'>
          <tr>
            <td style={{fontWeight: 'bold'}}>Nama Pelanggan</td>
            <td><input name='nama' style={{width: '200px'}}/></td>
          </tr>
          <tr>
            <td></td>
            <td>
              <InputCheckBox name='item1' val='Semangka' />
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <InputCheckBox name='item2' val='Jeruk' />
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <InputCheckBox name='item3' val='Nanas' />
            </td>
          </tr>
          <tr>
            <td></td>
            <td>
              <InputCheckBox name='item4' val='Salak' />
            </td>
          </tr>
          <tr>
            <td style={{fontWeight: 'bold'}}>Daftar Item</td>
            <td>
              <InputCheckBox name='item5' val='Anggur' />
            </td>
          </tr>
          <tr>
            <input type="submit" value="Kirim" />
          </tr>
        </table>
      </form>
    </div>
    </div>
  )
}

export default Tugas9
