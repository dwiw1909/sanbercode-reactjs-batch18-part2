import React, {useContext} from 'react';
import {BuahContext} from './BuahContext'
import Axios from 'axios'

const Form = () => {
  const [form, setForm, edit, setEdit, , , , setFetch] = useContext(BuahContext)
  const onChange = (key, val) => {
    setForm({
      ...form,
      [key]: val
    })
  }

  const save = (event) => {
    event.preventDefault()
    if(form.name !== '' && form.price !== '' && form.weight !== ''){
      if(!edit){
        Axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {...form})
        .then((result) => {
          setFetch(true)
          clear()
          console.log('berhasil save');
        }).catch((err) => {
          console.log(err);
        });
      } else {
        Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${form.id}`, {...form})
        .then((result) => {
          setFetch(true)
          clear()
          console.log('berhasil update');
        }).catch((err) => {
          console.log(err);
        });
      }
    } else {
      alert('input form harus di isi semua')
    }
  }

  const clear = () => {
    setForm({name: '', price: '', weight: ''})
    setEdit(false)
  }

  return (
    <div>
      <h2>Form Input</h2>
      <div className='form'>
        <form onSubmit={save}>
          <label>Nama</label>
          <input type='text' value={form.name|| ''} onChange={value => onChange('name', value.target.value)}/><br /><br />
          <label>Harga</label>
          <input type='text' value={form.price|| ''} onChange={value => onChange('price', value.target.value)} /><br /><br />
          <label>Berat</label>
          <input type='text' value={form.weight|| ''} onChange={value => onChange('weight', value.target.value)} /> gram<br /><br />
          <div style={{display: 'flex'}}>
          {
            edit
            ?<input className='submitBtn' type='submit' value='Edit' />
            :<input className='submitBtn' type='submit' value='Simpan' />
          }
          </div>
        </form>
      </div>
    </div>
  );
}

export default Form;