import React from 'react';
import './Tugas14.css';
// import {BuahProvider} from './BuahContext'
import Table from './Table'
import Form from './Form'

const Tugas12 = () => {
  
  return (
    // <BuahProvider>
      <div className='container'>
        <Form />
        <Table />
      </div>
    // </BuahProvider>
  );
}

export default Tugas12;