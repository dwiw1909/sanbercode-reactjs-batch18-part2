import React, {useState, createContext} from 'react';
export const BuahContext = createContext()

export const BuahProvider = props => {
  const [form, setForm] = useState({name: '', price: '', weight: ''})
  const [edit, setEdit] = useState(false)
  const [fetch, setFetch] = useState(true)
  const [dataHargaBuah, setDataHargaBuah] = useState([])
  const [darkMode, setDarkMode] = useState(false)
  return (
      <BuahContext.Provider value={[form, setForm, edit, setEdit, dataHargaBuah, setDataHargaBuah, fetch, setFetch, darkMode, setDarkMode]} >
        {props.children}
      </BuahContext.Provider>
  );
};
