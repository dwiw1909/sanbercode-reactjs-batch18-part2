import React, {useContext, useEffect} from 'react'
import {BuahContext} from './BuahContext'
import TableItem from './TableItem'
import Axios from 'axios'

const Table = () => {
  const [, setForm, , setEdit, dataHargaBuah, setDataHargaBuah, fetch, setFetch] = useContext(BuahContext)

  useEffect(() => {
    if(fetch){
      fetchData();
    }
  })

  const fetchData = () => {
    Axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(({data}) => {
      setFetch(false)
      setDataHargaBuah(data)
    }).catch((err) => {
      console.log(err);
    });
  }
  
  const del = (id) => {
    Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
    .then((result) => {
      fetchData()
    }).catch((err) => {
      console.log(err);
    });
  }

  const handleEdit = (id) => {
    Axios.get(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
    .then(({data}) => {
      setEdit(true)
      setForm(data)
    }).catch((err) => {
      console.log(err);
    });
  }

  return (
    <div>
      <h2>Table Harga Buah</h2>
        <div className='body'>
          <table className='tugas10'>
            <thead>
              <tr>
                <th>Nama</th>
                <th>Harga</th>
                <th>Berat</th>
                <th>Aksi</th>
              </tr>
            </thead>
            <tbody>
              {dataHargaBuah.map((data) => (
                <TableItem item={data} key={data.id} del={() => del(data.id)} edit={() => handleEdit(data.id)} />)
              )}
            </tbody>
          </table>
        </div>
    </div>
  )
}
export default Table;