import React from 'react'
import {
  Route,
  Switch
} from 'react-router-dom'

import Tugas9 from '../Tugas-9/Tugas9'
import Tugas10 from '../Tugas-10/Tugas10'
import Tugas11 from '../Tugas-11/Tugas11'
import Tugas12 from '../Tugas-12/Tugas12'
import Tugas13 from '../Tugas-13/Tugas13'
import Tugas14 from '../Tugas-14/Tugas14'

const Routes = () => {
  return (
    <Switch>
      <Route path='/tugas9' component={Tugas9} />
      <Route path='/tugas10' component={Tugas10} />
      <Route path='/tugas11' component={Tugas11} />
      <Route path='/tugas12' component={Tugas12} />
      <Route path='/tugas13' component={Tugas13} />
      <Route path='/tugas14' component={Tugas14} />
    </Switch>
  )
}

export default Routes
