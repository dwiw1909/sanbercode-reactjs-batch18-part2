/* eslint-disable react-hooks/rules-of-hooks */
import React, { useContext } from 'react'
import { BrowserRouter as Router } from 'react-router-dom'
import {BuahContext} from '../Tugas-14/BuahContext'
import Routes from './Routes'
import Nav from './Nav'

const index = () => {
  const [, , , , , , , , darkMode] = useContext(BuahContext)
  return (
    <div className={darkMode ? 'dark-mode' : 'light-mode'}>
      <Router>
        <Nav />
        <Routes />
      </Router>
    </div>
  )
}

export default index
