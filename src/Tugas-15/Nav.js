import React, {Fragment, useContext} from 'react'
import {Link} from 'react-router-dom'
import {BuahContext} from '../Tugas-14/BuahContext'

import './Nav.css'

const Nav = () => {
  const [, , , , , , , , darkMode, setDarkMode] = useContext(BuahContext)

  const themeMode = () => {
    setDarkMode(!darkMode)
  }
  return (
    <Fragment>
      <ul>
        <li><Link to='/tugas9' className='a'>Tugas9</Link></li>
        <li><Link to='/tugas10' className='a'>Tugas10</Link></li>
        <li><Link to='/tugas11' className='a'>Tugas11</Link></li>
        <li><Link to='/tugas12' className='a'>Tugas12</Link></li>
        <li><Link to='/tugas13' className='a'>Tugas13</Link></li>
        <li><Link to='/tugas14' className='a'>Tugas14</Link></li>
        <li style={{float: 'right', padding: '14px 16px', color: 'white'}}>
          <div>
            <input type="button" value='Dark Mode' onClick={() => themeMode()}/>
          </div>
        </li>
      </ul>
    </Fragment>
  )
}

export default Nav
