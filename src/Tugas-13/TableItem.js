import React from 'react'

const TableItem = ({item, del, edit}) => {
  return (
    <tr>
      <td>{item.name}</td>
      <td>{item.price}</td>
      <td>{item.weight/1000} kg</td>
      <td style={{width: '100px', alignItems: 'center'}}>
        <button type='button' onClick={() => edit()}>Edit</button>
        <button type='button' onClick={() => del()}>Hapus</button>
      </td>
    </tr>
  )
}

export default TableItem
