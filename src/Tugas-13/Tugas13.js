import React, { useState, useEffect } from 'react';
import TableItem from './TableItem';
import './Tugas13.css';
import Axios from 'axios';

const Tugas12 = () => {
  const [form, setForm] = useState({name: '', price: '', weight: ''})
  const [edit, setEdit] = useState(false)
  
  const [dataHargaBuah, setDataHargaBuah] = useState([])

  useEffect(() => {
    fetchData()
  }, [])

  const fetchData = () => {
    Axios.get(`http://backendexample.sanbercloud.com/api/fruits`)
    .then(({data}) => {
      console.log(data);
      setDataHargaBuah(data)
    }).catch((err) => {
      console.log(err);
    });
  }

  const clear = () => {
    setForm({name: '', price: '', weight: ''})
    setEdit(false)
  }

  const onChange = (key, val) => {
    setForm({
      ...form,
      [key]: val
    })
  }

  const save = (event) => {
    event.preventDefault()
    if(form.name !== '' && form.price !== '' && form.weight !== ''){
      if(!edit){
        Axios.post(`http://backendexample.sanbercloud.com/api/fruits`, {...form})
        .then((result) => {
          fetchData()
          clear()
          console.log('berhasil save');
        }).catch((err) => {
          console.log(err);
        });
      } else {
        Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${form.id}`, {...form})
        .then((result) => {
          fetchData()
          clear()
          console.log('berhasil update');
        }).catch((err) => {
          console.log(err);
        });
      }
    } else {
      alert('input form harus di isi semua')
    }
  }

  const del = (id) => {
    Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${id}`)
    .then((result) => {
      console.log(result);
      fetchData()
    }).catch((err) => {
      console.log(err);
    });
  }

  const handleEdit = (id) => {
    setEdit(true)
    setForm(dataHargaBuah.find((x => x.id === id)))
  }

  return (
    <div className='container'>
      <h2>Form Input</h2>
      <div className='form'>
        <form onSubmit={save}>
          <label>Nama</label>
          <input type='text' value={form.name|| ''} onChange={value => onChange('name', value.target.value)}/><br /><br />
          <label>Harga</label>
          <input type='text' value={form.price|| ''} onChange={value => onChange('price', value.target.value)} /><br /><br />
          <label>Berat</label>
          <input type='text' value={form.weight|| ''} onChange={value => onChange('weight', value.target.value)} /> gram<br /><br />
          <div style={{display: 'flex'}}>
          {
            edit
            ?<input className='submitBtn' type='submit' value='Edit' />
            :<input className='submitBtn' type='submit' value='Simpan' />
          }
          </div>
        </form>
      </div>
      <h2>Table Harga Buah</h2>
      <div className='body'>
        <table className='tugas10'>
          <thead>
            <tr>
              <th>Nama</th>
              <th>Harga</th>
              <th>Berat</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {dataHargaBuah.map((data) => (
              <TableItem item={data} key={data.id} del={() => del(data.id)} edit={() => handleEdit(data.id)} />)
            )}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default Tugas12;