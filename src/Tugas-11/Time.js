import React, { Component } from 'react';

class Time extends Component {
  constructor(props){
    super(props)
    this.state = {
      time : '',
    }
    this.timer = null
  }

  componentDidMount(){
    this.startTimer()
  }
  
  startTimer = () => {
    this.timer = setInterval(this.getTime, 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.timer)
  }

  getTime = () => {
    if(this.props.time === false){
      this.componentWillUnmount()
    } else {
      let date = new Date()
      let seconds = date.getSeconds();
      let minutes = date.getMinutes();
      let hour = date.getHours();
      let sign = 'AM'
      if(hour > 12){
        sign = 'PM'
        hour -= 12
      }
      if(minutes < 10){
        minutes = `0${minutes}`
      }
      if(seconds < 10){
        seconds = `0${seconds}`
      }
      let now = `${hour}:${minutes}:${seconds} ${sign}`
      this.setState({
        time: now
      })
    }
  }

  render() {
    return (
      <div>
        <h2>Sekarang Jam : {this.state.time}</h2>
      </div>
    );
  }
}

export default Time;