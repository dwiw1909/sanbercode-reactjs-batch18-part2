import React, { Component } from 'react';

class Countdown extends Component {
  constructor(props){
    super(props)
    this.state = {
      number: 101
    }
    this.counter = null
  }

  componentDidMount(){
    this.startCountdown()
  }

  startCountdown = () => {
    this.counter = setInterval(this.count, 1000);
  }

  componentWillUnmount = () => {
    clearInterval(this.counter)
  }

  count = () => {
    if(this.state.number <= 0) {
      this.componentWillUnmount()
      this.props.stop()
    } else {
      let num = this.state.number -1
      this.setState({
        number: num
      })
    }
  }

  render() {
    return (
      <h2>Hitung Mundur {this.state.number}</h2>
    );
  }
}

export default Countdown;