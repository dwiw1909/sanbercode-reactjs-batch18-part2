import React, { Component } from 'react';
import Time from './Time.js'
import Countdown from './Countdown.js'

class Tugas11 extends Component {
  constructor(props){
    super(props)
    this.state = {
      active : true,
      time : true
    }
  }

  timeOut = () => {
    this.setState({
      time: false,
      active : false
    })
  }
  
  render() {
    return (
      <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'space-evenly', width: '70%', margin: '0px auto'}}>
        {
          this.state.active 
          ? 
          <>
            <Time time={this.state.time} />
            <Countdown stop={this.timeOut} />
          </>
          : null
        }
      </div>
    );
  }
}

export default Tugas11;